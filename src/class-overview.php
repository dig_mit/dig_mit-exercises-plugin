<?php
namespace DigmitExercises;

class Overview
{
  public $exercises = [];

  public function __construct () {
    global $wpdb;

    // load all exercises from database
    $r = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . DIGMIT_TABLE_EXERCISES);
    foreach ( $r as $row ) {
      $row->config = json_decode( $row->config );
      $this->exercises[] = $row;
    }
  }

  public static function render () {
    global $wpdb;
    $ov = new Overview();

    ?>
    <div class="wrap">
      <h1>dig_mit! Übungen: Übersicht</h1>

      <div id="digmit-exercise-notifications"></div>

      <table class="widefat striped">
        <thead>
          <tr>
            <th><?= __("ID", "digmit_exercises"); ?></th>
            <th><?= __("Anmerkungen", "digmit_exercises"); ?></th>
            <th><?= __("Titel", "digmit_exercises"); ?></th>
            <th><?= __("Typ", "digmit_exercises"); ?></th>
            <th><?= __("shortcode", "digmit_exercises"); ?></th>
            <th><?= __("Aktionen", "digmit_exercises"); ?></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach( $ov->exercises as $exercise ) : ?>
            <tr id="digmit-exercise-id-<?= $exercise->id ?>">
              <td><?= $exercise->id ?></td>
              <td><?= $exercise->note ?></td>
              <td><?= $exercise->config->title ?></td>
              <td><?= $exercise->type ?></td>
              <td>
                <span>[<?= DIGMIT_SHORTCODE ?> id="<?= $exercise->id ?>"]</span>
                <a title="<?= __("In die Zwischenablage kopieren", "digmit_exercises") ?>"><span
                  class="dashicons dashicons-migrate digmit-exercise-shortcode-copy"
                  data-code='[<?= DIGMIT_SHORTCODE ?> id="<?= $exercise->id ?>"]'
                ></span></a>
              </td>
              <td>
                <a href="<?= menu_page_url('digmit_menu_edit', false) . '&id=' . $exercise->id ?>" title="<?= __("Bearbeiten", "digmit_exercises"); ?>">
                  <span class="dashicons dashicons-edit"></span>
                </a>
                <a href="#" class="digmit-exercise-delete" title="<?= __("Löschen", "digmit_exercises"); ?>">
                  <span class="dashicons dashicons-trash" data-id="<?= $exercise->id ?>"></span>
                </a>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>

      <a href="<?php menu_page_url("digmit_menu_create") ?>"><button>Neue Übung anlegen</button></a>

      <input type="hidden" id="digmit-api-exercise" value="<?= get_rest_url( null, DIGMIT_API_ROOT . '/exercise' ) ?>">
      <input type="hidden" id="digmit-api-nonce" value="<?= wp_create_nonce( 'wp_rest' ) ?>">
    </div>
    <?php
  }

}
