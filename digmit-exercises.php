<?php
/**
 * Plugin Name:     dig_mit! exercises
 * Description:     Learning exercises for the dig_mit! project
 * Version:         0.1.0
 * Author:          Andrea Ida Malkah Klaura <jackie@tantemalkah.at>
 * License:         GPL-2.0-or-later
 * License URI:     https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:     digmit-exercises
 *
 * @package         digmit-exercises
 */

namespace DigmitExercises;

define ("DIGMIT_TABLE_EXERCISES", "digmit_exercises");
define ("DIGMIT_API_ROOT", "digmit/v1");
define ("DIGMIT_IMPLEMENTED_TYPES", array(
  "draggable-syllables",
	"syllable-puzzle",
	"listen-and-write",
  "lingua-puzzle",
));
define ("DIGMIT_SHORTCODE", "digmit_exercise");
define ("DIGMIT_DEFAULT_AUDIO_DIR", "/wp-content/uploads/digmit-audio-default/");

register_activation_hook( __FILE__, function() {
	require_once plugin_dir_path( __FILE__ ) . 'src/class-activation.php';
  Activation::activate();
} );

require_once plugin_dir_path( __FILE__ ) . 'src/class-plugin.php';

$digmitex = new Plugin();
$digmitex->init();
