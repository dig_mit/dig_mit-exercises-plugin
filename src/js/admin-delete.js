
DigMitExercise.delete = function ( id ) {
  const url = $( "#digmit-api-exercise" ).val() + '/' + id
  const nonce = $( "#digmit-api-nonce" ).val()

  this.clearNotifications()
  $.ajax({
    method: 'DELETE',
    url: url,
    contentType: 'application/json',
    headers: {
      "X-WP-Nonce": nonce,
    },
    success: ( response ) => {
      let msg = 'Exercise deleted!<br><br>'
      this.displayInfo( msg )
      let $row = $( "#digmit-exercise-id-"+id )
      $row.remove()
    },
    error: ( response ) => {
      let msg = 'Could not delete exercise. Please provide the following error to an administrator:<hr>'
      msg += 'Request URL: ' + url + '<br>'
      msg += 'Status: ' + response.status + ' ' + response.statusText + '<br>'
      msg += 'Code: ' + response.responseJSON.code + '<br>'
      msg += 'Message: ' + response.responseJSON.message
      this.displayError( msg )
    }
  })
}


DigMitExercise.initDelete = function () {
  $( ".digmit-exercise-delete" ).on( "click", event => {
    event.preventDefault()
    DigMitExercise.delete( $( event.target ).data( 'id' ) )
  } )
}
