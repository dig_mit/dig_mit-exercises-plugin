<?php
namespace DigmitExercises;

class Create
{

  public static function render () {
    ?>
    <div class="wrap" id="form-wrap">
      <h1>dig_mit! Übung anlegen</h1>

      <div id="digmit-exercise-notifications"></div>


      <table id="form">
        <tr>
          <td colspan="2" align="center">
            <input type="hidden" id="digmit-api-create" value="<?= get_rest_url( null, DIGMIT_API_ROOT . '/exercise' ) ?>">
            <input type="hidden" id="digmit-api-nonce" value="<?= wp_create_nonce( 'wp_rest' ) ?>">
            <input type="hidden" id="digmit-link-overview" value="<?php menu_page_url("digmit_menu") ?>">
            <input type="hidden" id="digmit-link-edit" value="<?php menu_page_url("digmit_menu_edit") ?>">
          </td>
        </tr>
      </table>
      <div>
  <hr>
        <a href="#" id="digmit-exercise-submit"><button>Anlegen</button></a>
      </div>
    </div>
    <?php
  }

}
