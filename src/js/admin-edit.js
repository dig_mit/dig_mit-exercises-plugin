
DigMitExercise.select = function ( id ) {
  // find the select exercise from the full list of exercises
  exercise = this.list.find( ex => ex.id === id )
  // update the exercise type and (re)build the form for this exercise
  $( '#digmit-exercise-type' ).val( exercise.type )
  this.buildForm( exercise )
}

DigMitExercise.edit = function () {
  // gather data from input elements
  const url = $( "#digmit-api-url" ).val()
  const nonce = $( "#digmit-api-nonce" ).val()
  const exerciseId = $( "#digmit-exercises-selector" ).val()

  // validate and parse the form contents
  this.validateForm()
  if (!this.validated) return
  this.parseForm()

  // now make the PUT request to update the exercise
  this.clearNotifications()
  $.ajax({
    method: 'PUT',
    url: url + '/' + exerciseId,
    contentType: 'application/json',
    headers: {
      "X-WP-Nonce": nonce,
    },
    data: JSON.stringify( this.exerciseData ),
    success: ( response ) => {
      // update the exercise in the local store and the option label
      let i = this.list.findIndex( ex => ex.id === exerciseId )
      this.list[i] = response
      $( "#digmit-exercises-selector option[value="+exerciseId+"]" ).text( exerciseId + ': ' + this.exerciseData.note )
      // output a success alert
      let msg = 'Exercise updated!<br><br>'
      msg += '<a href="' + $( "#digmit-link-overview" ).val() + '">Go to overview</a>'
      this.displayInfo( msg )
    },
    error: ( response ) => {
      let msg = 'Status: ' + response.status + ' ' + response.statusText + '<br>'
      msg += 'Code: ' + response.responseJSON.code + '<br>'
      msg += 'Message: ' + response.responseJSON.message
      this.displayError( msg )
    }
  })
}


DigMitExercise.initEdit = function () {
  const $selector = $( "#digmit-exercises-selector" )
  const url = $( "#digmit-api-url" ).val()

  // hide the editor table while we load all available exercises
  $( '#form' ).css( 'display', 'none' )
  $.get({
    url: url,
    success: ( response ) => {
      // store all available exercises locally
      this.list = response
      // mark either the first result as selected or get it from the GET parameters
      let selected = response[0].id
      if ( this.getParams.id !== undefined ) {
        selected = this.getParams.id
      }
      // append a selector option for every available exercise
      for ( let exercise of response ) {
        let value = exercise.id
        let label = exercise.id + ': ' + exercise.note
        let $option
        if ( selected === exercise.id ) {
          $option = $( '<option value="' + value + '" selected>' + label + '</option>' )
        } else {
          $option = $( '<option value="' + value + '">' + label + '</option>' )
        }
        $option.appendTo( $selector )
      }
      // prepare the exercise type field and disable it, as the type of an existing exercise
      // should not be changed accidentally
      DigMitExercise.renderTypeField();
      $('#digmit-exercise-type').prop('disabled', true)
      // now that everything is loaded activate the editor table with the selected exercises
      this.select( selected )
      // show the table again
      $( '#form' ).css( 'display', 'table' )
    },
    error: ( response ) => {
      let msg = 'Status: ' + response.status + ' ' + response.statusText + '<br>'
      msg += 'Code: ' + response.responseJSON.code + '<br>'
      msg += 'Message: ' + response.responseJSON.message
      this.displayError( msg )
    }
  })

  $( "#digmit-exercise-submit-edit" ).on( "click", event => {
    event.preventDefault()
    this.edit()
  })

  $( "#digmit-exercises-selector" ).on( "change", event => {
    this.clearNotifications()
    this.select( $( "#digmit-exercises-selector" ).val() )
  })
}
