<?php

namespace DigMitExercises;

class Exercise {

  /**
   * Render an exercise into its final HTML
   *
   * This is the main static function that is called through the shortcode.
   * It the generates the relevant HTML output for the exercise depending
   * on the exercise ID provided in the shortcode.
   *
   * @param array $attributes The shortcode attributes (should contain an "id")
   * @return string The rendered HTML output for this exercise
   */
  public static function render( $attributes ) {
    $html = '';

    if ( ! isset( $attributes["id"] ) ) {
      $html .= '
      <div class="error">
        No ID provided in your shortcode. Cannot find exercise.
      </div>
      ';
      return $html;
    }

    if ( ! preg_match( '/^[0-9]+$/', $attributes["id"] ) ) {
      $html .= '
      <div class="error">
        Only integer numbers are allowed as exercise ID.
      </div>
      ';
      return $html;
    }

    // now that it is validated, set the $id parameter
    $id = $attributes["id"];

    // fetch the exercise data from the database
    global $wpdb;
    $table = $wpdb->prefix . DIGMIT_TABLE_EXERCISES;
    $query = $wpdb->prepare( "SELECT * FROM $table WHERE id = %d", $id );
    $exercise = $wpdb->get_row( $query );

    if ( empty( $exercise ) ) {
      $html .= '
      <div class="error">
        This exercise does not exist!<br>
        <small>(The provided ID was: '.$id.')</small>
      </div>
      ';
      return $html;
    }

    $html .= '
    <div id="digmit-exercise-container-'.$exercise->id.'" class="digmit-exercise-container"></div>
    <script>
      let digMitExerciseID = '.$exercise->id.'
      let DigMitConfig = JSON.parse(\''.str_replace("'", "\\'", $exercise->config).'\')
    </script>
    <script src="'. plugin_dir_url( __FILE__ ) . 'js/ex-'. $exercise->type .'.js' .'"></script>
    ';
    return $html;
  }

}
