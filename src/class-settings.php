<?php
namespace DigmitExercises;

class Settings {
  /**
   * Output the dig_mit settings page.
   */
  public static function render () {
    ?>
    <div class="wrap">
      <h1>dig_mit! Einstellungen</h1>

      <?php
      if (isset($_POST["submit"])) {
        Settings::store();
      }
      ?>

      <form action="" method="post">
        <table>
          <tr>
            <td>Default Audio Directory:</td>
            <td><input type="text" name="digmit-default-audio-dir" value="<?= get_option("digmit_default_audio_dir", DIGMIT_DEFAULT_AUDIO_DIR); ?>" size="60" maxlength="255"></td>
          </tr>
          <tr>
            <td>Default Placeholder Page URL:</td>
            <td><input type="text" name="digmit-url-wip-placeholder" value="<?= get_option("digmit_url_wip_placeholder", '#'); ?>" size="60" maxlength="255"></td>
          </tr>
        </table>
        <hr>
        <input type="hidden" name="_wpnonce" value="<?= wp_create_nonce('digmit-settings') ?>">
        <input type="submit" name="submit" value="Speichern"></a>
      </form>
    </div>
    <?php
  }

  /**
   * Validate and store the settings as wordpress options.
   */
  public static function store () {
    if (empty($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'], 'digmit-settings')) {
      echo Plugin::render_error('Nonce missing or wrong!');
      return;
    }

    if (!empty($_POST['digmit-default-audio-dir'])) {
      $value = $_POST['digmit-default-audio-dir'];
      if (strlen($value) > 255) {
        echo Plugin::render_error('default audio dir too long!');
        return;
      }
      $value = sanitize_url($value);
      update_option('digmit_default_audio_dir', $value);
    }

    if (!empty($_POST['digmit-url-wip-placeholder'])) {
      $value = $_POST['digmit-url-wip-placeholder'];
      if (strlen($value) > 255) {
        echo Plugin::render_error('default placeholder page URL too long!');
        return;
      }
      $value = sanitize_url($value);
      update_option('digmit_url_wip_placeholder', $value);
    }

  }
}
