<?php
namespace DigmitExercises;

class Maintenance
{
  /**
   * Output the dig_mit maintenance page.
   */
  public static function render () {
    $maintenance = new Maintenance();

    // check if a maintenance action was performed
    if (isset($_GET['action'])) {
      $action = $_GET['action'];
      if ($action == 'clear_tags') {
        if (wp_verify_nonce($_GET['nonce'], 'clear_tags')) {
          $maintenance->clear_tags();
        } else {
          echo $maintenance->render_error('Invalid nonce!');
        }
      }

      elseif ($action == 'clear_nbsp') {
        if (wp_verify_nonce($_GET['nonce'], 'clear_nbsp')) {
          $maintenance->clear_nbsp();
        } else {
          echo $maintenance->render_error('Invalid nonce!');
        }
      }

      else {
        echo $maintenance->render_error('Ungültige Wartungs-Aktion angefordert!');
      }
    }


    $missing = $maintenance->check_missing_audio();
    $title_tag_pages = $maintenance->check_for_tags_in_title();
    $nbsp_pages = $maintenance->check_for_nbsp();
    ?>
    <div class="wrap">
      <h1>dig_mit! Wartungsarbeiten</h1>
      <h2>Folgende Audiofiles fehlen noch:</h2>
      <?php if ( empty( $missing ) ) : ?>
        <div class="success">
          <span class="dashicons dashicons-yes"></span>
          Alle benötigten Audiodateien sind bereits vorhanden.
        </div>
      <?php else: ?>
        <?php foreach ( $missing as $exercise ) : ?>
          <p>In Übung <b>[<?= $exercise["id"] ?>] <?= $exercise["note"] ?></b> :</p>
          <ul>
            <?php foreach ( $exercise["words"] as $word ) : ?>
              <li><?= $exercise["audioSource"] . $word ?><?= $exercise["type"] == "lingua-puzzle" ? "" : ".mp3" ?></li>
            <?php endforeach; ?>
          </ul>
          <?php if ( $exercise["type"] != "lingua-puzzle" ) : ?>
          <p>
            Als Leerzeichen-getrennte Wortliste:
            <code>
              <?php
              foreach ( $exercise["words"] as $word ) {
                echo $word . ' ';
              }
              ?>
            </code>
          </p>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>

      <h2>In folgenden Artikeln enthält der Title HTML-Tags:</h2>
      <?php if ( empty( $title_tag_pages ) ) : ?>
        <div class="success">
          <span class="dashicons dashicons-yes"></span>
          Keine Seiten mit HTML-Tags gefunden.
        </div>
      <?php else: ?>
        <ul>
          <?php foreach ( $title_tag_pages as $page ) : ?>
            <li><?= $page->ID . ' : <a href="'.get_permalink($page->ID).'"><code>'.esc_html($page->post_title).'</code></a>' ?></li>
          <?php endforeach; ?>
        </ul>
        <a href="<?= menu_page_url('digmit_menu_maintenance', false) . '&action=clear_tags&nonce='.wp_create_nonce('clear_tags') ?>">
          <button>Tags entfernen</button>
        </a>
      <?php endif; ?>

      <h2>Folgende Artikel wurden mit non-breaking spaces (<code>&amp;nbsp;</code>) gefunden:</h2>
      <?php if ( empty( $nbsp_pages ) ) : ?>
        <div class="success">
          <span class="dashicons dashicons-yes"></span>
          Keine Seiten mit <code>&amp;nbsp;</code> gefunden.
        </div>
      <?php else: ?>
        <ul>
          <?php foreach ( $nbsp_pages as $page ) : ?>
            <li><?= $page->ID . ' : <a href="'.get_permalink($page->ID).'">'.strip_tags($page->post_title).'</a>' ?></li>
          <?php endforeach; ?>
        </ul>
        <a href="<?= menu_page_url('digmit_menu_maintenance', false) . '&action=clear_nbsp&nonce='.wp_create_nonce('clear_nbsp') ?>">
          <button>Alle <code>&amp;nbsp;</code> in einfache Leerzeichen umwandeln</button>
        </a>
      <?php endif; ?>
    </div>
    <?php
  }


  /**
   * Return a formatted error div for a text message
   *
   * @param string $text An error message to be rendered
   * @return string The rendered error message
   */
  private function render_error ($text) {
    return '
      <div class="error">
        <span class="dashicons dashicons-warning"></span>
        '.$text.'
      </div>
    ';
  }


  /**
   * Check all pages for occurences of &nbsp;
   *
   * @return array A list of all found pages
   */
  private function check_for_nbsp () {
    $found = [];
    $all = get_pages();
    foreach ( $all as $page ) {
      if (str_contains($page->post_content, '&nbsp;')) array_push($found, $page);
    }
    return $found;
  }


  /**
   * Check all pages for tags in the title;
   *
   * @return array A list of all found pages
   */
  private function check_for_tags_in_title () {
    $found = [];
    $all = get_pages();
    foreach ( $all as $page ) {
      if (preg_match('/\<\/?.+\>/', $page->post_title)) array_push($found, $page);
    }
    return $found;
  }


  /**
   * Clear all tags from page titles
   *
   * @return void
   */
  private function clear_tags () {
    $pages = $this->check_for_tags_in_title();
    foreach ($pages as $page) {
      $page->post_title = strip_tags($page->post_title);
      wp_update_post($page);
    }
  }


  /**
   * Clear all &nbsp; from page content
   *
   * @return void
   */
  private function clear_nbsp () {
    $pages = $this->check_for_nbsp();
    foreach ($pages as $page) {
      $page->post_content = str_replace('&nbsp;', ' ', $page->post_content);
      wp_update_post($page);
    }
  }


  /**
   * Check all exercises for missing audio files.
   *
   * @return array A list exercises for which fiels are missing, including a
   *               list of missing words/syllables/sounds.
   */
  public function check_missing_audio () {
    // fetch all exercises from the database
    global $wpdb;
    $table = $wpdb->prefix . DIGMIT_TABLE_EXERCISES;
    $exercises = $wpdb->get_results( "SELECT * FROM $table" );

    // in this array we collect all missing words sorted by exercise
    $missing = [];

    foreach ( $exercises as $exercise ) {
      // the list of missing words will be filled in here (if any)
      $words = [];

      // check all words in the exercise
      $config = json_decode( $exercise->config );
      switch ( $exercise->type ) {
        case "draggable-syllables" :
          $words = $this->check_words_draggable_syllables( $config );
          break;

        case "listen-and-write" :
          $words = $this->check_words_listen_and_write( $config );
          break;

        case "syllable-puzzle" :
          $words = $this->check_words_syllable_puzzle( $config );
          break;

        case "lingua-puzzle":
          $words = $this->check_words_lingua_puzzle( $config );

      }

      // if any words have been found, add them to the missing words array
      // including the exercise's meta infos
      if ( ! empty( $words ) ) {
        $missing[] = array(
          "id" => $exercise->id,
          "type" => $exercise->type,
          "note" => $exercise->note,
          "audioSource" => $config->audioSource,
          "words" => $words,
        );
      }
    }
    return $missing;
  }


  /**
   * Check for all missing audio files of a draggable syllables exercise.
   *
   * @param object $config The full exercise configuration
   * @return array A list of words/syllables for which no audio file exists
   */
  private function check_words_draggable_syllables ( $config ) {
    $words = $config->words;
    $base = get_home_path() . $config->audioSource;

    $missing = [];
    foreach ( $words as $word ) {
      if ( ! file_exists( $base . $word->label . '.mp3' ) ) {
        $missing[] = $word->label;
      }
      if ( count( $word->syllables ) > 1 ) {
        foreach ( $word->syllables as $syllable ) {
          if ( ! file_exists( $base . $syllable . '.mp3' ) ) {
            $missing[] = $syllable;
          }
        }
      }
    }
    // check for additional audio labels in title and target areas
    if ( $config->titleAudio->enabled ) {
      if ( ! file_exists( $base . $config->titleAudio->label . '.mp3' ) ) {
        $missing[] = $config->titleAudio->label;
      }
    }
    foreach ( $config->answers as $answer ) {
      if ( $answer->hasAudio ) {
        if ( ! file_exists( $base . $answer->label . '.mp3' ) ) {
          $missing[] = $answer->label;
        }
      }
    }

    return array_unique( $missing );
  }


  /**
   * Check for all missing audio files of a listen and write exercise.
   *
   * @param object $config The full exercise configuration.
   * @return array A list of words/sounds for which no audio file exists.
   */
  private function check_words_listen_and_write ( $config ) {
    $words = $config->words;
    $base = get_home_path() . $config->audioSource;

    $missing = [];
    foreach ( $words as $word ) {
      if ( ! file_exists( $base . $word->full . '.mp3' ) ) {
        $missing[] = $word->full;
      }
      foreach ( $word->sounds as $sound ) {
        if ( ! file_exists( $base . $sound . '.mp3' ) ) {
          $missing[] = $sound;
        }
        // if a sound consists of more than one character we also have
        // to check for each single character sound
        if ( mb_strlen( $sound ) > 1 ) {
          for ( $i = 0; $i < mb_strlen( $sound ); $i++ ) {
            if ( ! file_exists( $base . mb_substr( $sound, $i, 1 ) . '.mp3' ) ) {
              $missing[] = $sound[mb_substr( $sound, $i, 1 )];
            }
          }
        }
      }
    }

    return array_unique( $missing );
  }


  /**
   * Check for all missing audio files of a syllable puzzle
   *
   * @param object $config The full exercise configuration
   * @return array A list of words/syllables for which no audio file exists
   */
  private function check_words_syllable_puzzle ( $config ) {
    $words = $config->words;
    $base = get_home_path() . $config->audioSource;

    $missing = [];
    foreach ( $words as $word ) {
      if ( ! file_exists( $base . $word->full . '.mp3' ) ) {
        $missing[] = $word->full;
      }
      if ( count( $word->syllables ) > 1 ) {
        foreach ( $word->syllables as $syllable ) {
          if ( ! file_exists( $base . $syllable . '.mp3' ) ) {
            $missing[] = $syllable;
          }
        }
      }
    }

    return array_unique( $missing );
  }


  /**
   * Check for all missing audio files of a lingua puzzle
   *
   * @param object $config The full exercise configuration
   * @return array A list of audio files which are missing
   */
  private function check_words_lingua_puzzle ( $config ) {
    $texts = $config->words;
    $base = get_home_path() . $config->audioSource;

    $missing = [];
    foreach ( $texts as $text ) {
      if ( ! file_exists( $base . $text->file ) ) {
        $missing[] = $text->file;
      }
    }

    return array_unique( $missing );
  }

}
