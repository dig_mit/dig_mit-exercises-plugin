<?php
namespace DigmitExercises;

class Activation {
  public static function activate () {

    // set up the database tables
    global $wpdb;

    $tbl_exercises = $wpdb->prefix . DIGMIT_TABLE_EXERCISES;

    $charset_collate = $wpdb->get_charset_collate();

    $sql = [];
    $sql[] = "CREATE TABLE $tbl_exercises (
      id INT NOT NULL AUTO_INCREMENT,
      type VARCHAR(48),
      config TEXT,
      note TEXT,
      PRIMARY KEY  (id)
    ) $charset_collate;
    ";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );

    // add default options for plugin configuration (with autoloading turned off)
    add_option('digmit_default_audio_dir', DIGMIT_DEFAULT_AUDIO_DIR, '', false);
    add_option('digmit_url_wip_placeholder', '#', '', false);
  }
}
