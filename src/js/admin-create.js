/**
 * Assembles exercise config based on form fields and sends API request
 * to create new exercise.
 */
DigMitExercise.create = function () {
  // gather data from input elements
  const url = $( "#digmit-api-create" ).val()
  const nonce = $( "#digmit-api-nonce" ).val()

  // validate and parse the form contents
  this.validateForm()
  if (!this.validated) return
  this.parseForm()

  // now make the POST request to create the new exercise
  this.clearNotifications()
  $.post({
    url: url,
    contentType: 'application/json',
    headers: {
      "X-WP-Nonce": nonce,
    },
    data: JSON.stringify( this.exerciseData ),
    success: ( response ) => {
      let msg = 'Exercise created with ID ' + response.id + '!<br><br>'
      msg += '<a href="' + $( "#digmit-link-edit" ).val() + '&id=' + response.id + '">Edit</a> &nbsp; | &nbsp; '
      msg += '<a href="' + $( "#digmit-link-overview" ).val() + '">Go to overview</a>'
      this.displayInfo( msg )
      window.scrollTo(0, 0)
    },
    error: ( response ) => {
      let msg = 'Status: ' + response.status + ' ' + response.statusText + '<br>'
      msg += 'Code: ' + response.responseJSON.code + '<br>'
      msg += 'Message: ' + response.responseJSON.message
      this.displayError( msg )
      window.scrollTo(0, 0)
    }
  })
}


/**
 * Initialises the form fields to create a new exercise
 */
DigMitExercise.initCreate = function () {
  // render the type field
  DigMitExercise.renderTypeField();

  // build the form fields whenever the exercise type changes
  $( "#digmit-exercise-type" ).on( "change", function() {
    DigMitExercise.buildForm()
  } )

  // use the create() method when the submit button is clicked
  $( "#digmit-exercise-submit" ).on( "click", event => {
    event.preventDefault()
    DigMitExercise.create()
  } )
}
