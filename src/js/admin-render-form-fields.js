DigMitExercise.renderTypeField = function () {
    const types = ["syllable-puzzle", "draggable-syllables", "listen-and-write", "lingua-puzzle"]
    let options = ""
    types.forEach(type => { options += `<option value="` + type + `">` + type + `</option>` })

    let typeField = `
      <tr>
        <td>
          <label for="digmit-exercise-type">Übungstyp</label>
        </td>
        <td>
          <select name="type" id="digmit-exercise-type">
            <option value="none" selected disabled hidden> Wähle eine Übung </option>`
        + options +
        `</select>
          <br>
        </td>
      </tr>
    `
    $(typeField).appendTo("#form")
}

DigMitExercise.renderAnswersField = function () {
    let answersField = `
      <tr>
        <td>
          <label for="digmit-exercise-answer1">Antwort 1</label>
        </td>
        <td>
          <input type="text" id="digmit-exercise-answer1"></input>
          <label for="digmit-exercise-hasAudio1"> mit Audio?</label>
          <input type="checkbox" name="digmit-exercise-hasAudio1" id="digmit-exercise-hasAudio1">
        </td>

        <td>
          <label for="digmit-exercise-answer2">Antwort 2</label>
        </td>
        <td>
           <input type="text" id="digmit-exercise-answer2"></input>
           <label for="digmit-exercise-hasAudio2"> mit Audio?</label>
           <input type="checkbox" name="digmit-exercise-hasAudio2" id="digmit-exercise-hasAudio2">
        </td>
      </tr>
    `
    $(answersField).appendTo("#form")
}

DigMitExercise.renderTitleaudioField = function () {
    let answersField = `
      <tr>
        <td>
          <label for="digmit-exercise-titleAudio">Audio im Titel</label>
        </td>
        <td>
          <input type="checkbox" name="digmit-exercise-titleAudioEnabled" id="digmit-exercise-titleAudioEnabled">
          <label for="digmit-exercise-titleAudioEnabled"> Aktivieren</label>
          <input type="text" id="digmit-exercise-titleAudio" placeholder="Label"></input>
        </td>
      </tr>
    `
    $(answersField).appendTo("#form")
}

DigMitExercise.renderTextField = function (id, text, placeholder) {
    let textField = `
      <tr>
        <td>
          <label for="` + id + `">` + text + `</label>
        </td>
        <td>
          <input type="text" id="` + id + `" placeholder="` + placeholder + `">
        </td>
      </tr>
    `
    $(textField).appendTo("#form")
}

DigMitExercise.renderTextareaField = function (id, text, placeholder) {
    let textField = `
      <tr>
        <td>
          <label for="` + id + `">` + text + `</label>
        </td>
        <td>
          <textarea id="` + id + `" placeholder="` + placeholder + `" rows="3" cols="40"> </textarea><br>
        </td>
      </tr>
    `
    $(textField).appendTo("#form")
}

DigMitExercise.renderRadioAnswer = function (wid) {
  let radioAnswer = `
    <td>
      <label for="digmit-exercise-answerRadio1">Antwort 1</label>
      <input type="radio" name="radio-answers-` + wid + `" id="digmit-exercise-answerRadio1-` + wid +`" class="digmit-exercise-answerRadio1"></input>
      <label for="digmit-exercise-answerRadio2">2</label>
      <input type="radio" name="radio-answers-` + wid + `" id="digmit-exercise-answerRadio2-` + wid +`" class="digmit-exercise-answerRadio2"></input>
    </td>
  `
  $(radioAnswer).insertBefore($("#digmit-exercise-silben-" + wid))
}

DigMitExercise.renderAudioFileInput = function (wid) {
  let inputField = `
    <td>
      <label for="">Audio file</label>
      <input type="text" id="digmit-exercise-audioSource-${wid}" class="digmit-exercise-audioSource"></input>
    </td>
  `
  $(inputField).insertBefore($("#digmit-exercise-silben-" + wid))
}

DigMitExercise.renderShowFullWordCheckbox = function (wid) {
  let checkboxField = `
  <tr>
    <td>
      <label for="digmit-exercise-showFullWord">Ganzes Wort anhörbar?</label>
    </td>
    <td>
      <input type="checkbox" name="digmit-exercise-showFullWord" id="digmit-exercise-showFullWord">
      (Ankreuzen bei "Wort hören &amp; schreiben"-Übungen, nicht aber für "Laute hören &amp; Wort schreiben")
    </td>
  </tr>
  `
  $(checkboxField).appendTo("#form")
}

DigMitExercise.addSyllableField = function (wid) {
    const sid = $(`#digmit-exercise-word-row-`+ wid).find(".digmit-exercise-syllable").length
    let textField = `
        <td>
            <input type="text" id="digmit-exercise-syllable-${wid}-${sid}" class="digmit-exercise-syllable" size="1">
        </td>
    `
    let field = $(`#digmit-exercise-word-row-` + wid)
    $(textField).appendTo(field)
}

DigMitExercise.addWordRow = function () {
    const fullLabel = $( "#digmit-exercise-type" ).val()  === "lingua-puzzle" ? "Text" : "Wort"
    const partLabel = $( "#digmit-exercise-type" ).val()  === "lingua-puzzle" ? "Segmente" : "Silben"
    const wid = $("#word-table").find(".digmit-exercise-word-row").length
    let textField = `
      <tr id="digmit-exercise-word-row-` + wid + `" class="digmit-exercise-word-row">
        <td>
          <label for="digmit-exercise-word-` + wid + `">${fullLabel}</label>
        </td>
        <td>
          <input type="text" id="digmit-exercise-word-` + wid + `" class="digmit-exercise-word"></input>
        </td>
        <td id="digmit-exercise-silben-` + wid + `">
          <label>${partLabel}</label>
        </td>
        <td>
          <a href="#" id="digmit-exercise-addSyllable-` + wid + `"><button><span class="dashicons dashicons-plus"</span></button></a>
          <a href="#" id="digmit-exercise-deleteSyllable-` + wid + `" title="Löschen"><button><span class="dashicons dashicons-minus"</span></button></a>
        </td>
        <td>
          <input type="text" id="digmit-exercise-syllable-` + wid + `-0" class="digmit-exercise-syllable" size="1">
        </td>
      </tr>
    `
    $(textField).appendTo("#word-table")

    // some exercises types need additional cells in the word row
    const exerciseType = $("#digmit-exercise-type").val()
    if ( exerciseType === "draggable-syllables" && $("#word-table").find("#digmit-exercise-answerRadio1-" + wid).length === 0 ) {
      this.renderRadioAnswer(wid)
    } else if ( exerciseType === "lingua-puzzle" ) {
      this.renderAudioFileInput(wid)
    }

    $('#digmit-exercise-addSyllable-' + wid).on("click", event => {
      event.preventDefault()
      DigMitExercise.addSyllableField(wid)
    })

    $('#digmit-exercise-deleteSyllable-' + wid ).on('click', event => {
      event.preventDefault()
      $(`#digmit-exercise-word-row-${wid}`).find('.digmit-exercise-syllable').last().closest('td').remove()
    })
}


DigMitExercise.renderWordField = function () {
    let wordField = `
      <table id="word-table">
        <h2>
          Worte hinzufügen
          <a href="#" id="digmit-exercise-addWord"><button>Wort <span class="dashicons dashicons-plus"</span></button></a>
          <a href="#" id="digmit-exercise-deleteWord"><button>Wort <span class="dashicons dashicons-minus"</span></button></a>
        </h2>
      </table>
    `
    $(wordField).insertAfter("#form");
    DigMitExercise.addWordRow();

    $("#digmit-exercise-addWord").on("click", event => {
        event.preventDefault()
        DigMitExercise.addWordRow()
    })

    $("#digmit-exercise-deleteWord").on('click', () => {
        $('#word-table tr:last').remove();
    })
}

DigMitExercise.submitButton = function () {
    let submitButton = `
      <div>
        <a href="#" id="digmit-exercise-submit"><button>Anlegen</button></a>
      </div>
    `
    $(submitButton).insertAfter("#form")
}


/**
 * This array configures the form fields used based on exercise types. The form fields are added in the order
 * as they are listed in the array. An optional `limitTo` property can be used as an array containing the
 * exercise types for which the field should be rendert. If the property is empty or missing, all exercises
 * will use the field.
 */
DigMitExercise.formFieldConfig = [
  {id: 'digmit-exercise-note', func: DigMitExercise.renderTextField, params: ["digmit-exercise-note", "Anmerkung", "Thema und Übung"]},
  {id: 'digmit-exercise-title', func: DigMitExercise.renderTextField, params: ["digmit-exercise-title", "Titel", "Titel"]},
  {id: 'digmit-exercise-description', func: DigMitExercise.renderTextField, params: ["digmit-exercise-description", "Beschreibung", "Beschreibung"]},
  {id: 'digmit-exercise-nextLabel', func: DigMitExercise.renderTextField, params: ["digmit-exercise-nextLabel", "Weiter am Ende der Übung", "Buttontext"]},
  {id: 'digmit-exercise-next', func: DigMitExercise.renderTextField, params: ["digmit-exercise-next", "Link am Ende der Übung", "Link für Button"]},
  {id: 'digmit-exercise-nextWordLabel', func: DigMitExercise.renderTextField, params: ["digmit-exercise-nextWordLabel", "Nächstes Wort", "Buttontext"], limitTo: ['syllable-puzzle', 'listen-and-write', 'lingua-puzzle']},
  {id: 'digmit-exercise-audioSource', func: DigMitExercise.renderTextField, params: ["digmit-exercise-audioSource", "Audio Source", "Ordner"]},
  {id: 'digmit-exercise-showFullWord', func: DigMitExercise.renderShowFullWordCheckbox, params: [], limitTo: ['listen-and-write']},
  {id: 'digmit-exercise-titleAudio', func: DigMitExercise.renderTitleaudioField, params: [], limitTo: ['draggable-syllables']},
  {id: 'digmit-exercise-answer1', func: DigMitExercise.renderAnswersField, params: [], limitTo: ['draggable-syllables']},
  {id: 'word-table', func: DigMitExercise.renderWordField, params: []},
]


/**
 * Builds the exercise form by rendering all necessary fields based on the exercise type and the
 * configuration in `DigMitExercise.formFieldConfig`.
 */
DigMitExercise.buildForm = function ( exercise ) {
  const exerciseType = $("#digmit-exercise-type").val()

  // go through all configured fields in the order they where configured
  for (field of this.formFieldConfig) {
    // if the current field is limited to certain exercises, and the current exercise
    // type is not found in the list, continue with the next field
    if (field.limitTo && !field.limitTo.find(item => item === exerciseType)) {
      // but delete the element first, if it exists
      $(`#${field.id}`).closest('tr').remove()
      continue
    }

    // for generic fields used in all exercise types just check whether the field is already there
    // and otherwise add it
    let $element = $(`#${field.id}`)
    if ($element.length === 0) {
      field.func(...field.params)
      // update the element selector, so it can be used later on
      $element = $(`#${field.id}`)
    }

    // if an existing exercise was selected, fill in the existing data
    if ( exercise ) {
      if (field.func === DigMitExercise.renderTextField) {
        let fieldLabel = field.id.split('-')[2]
        let fieldValue = fieldLabel === 'note' ? exercise[fieldLabel] : exercise.config[fieldLabel]
        $element.val( fieldValue )
      }
      else if (field.func === DigMitExercise.renderShowFullWordCheckbox) {
        $('#digmit-exercise-showFullWord').prop('checked', exercise.config.showFullWord)
      }
      else if (field.func === DigMitExercise.renderTitleaudioField) {
        $element.val( exercise.config.titleAudio.label )
        $('#digmit-exercise-titleAudioEnabled').prop('checked', exercise.config.titleAudio.enabled)
      }
      else if (field.func === DigMitExercise.renderAnswersField) {
        $('#digmit-exercise-answer1').val( exercise.config.answers[0].label )
        $('#digmit-exercise-answer2').val( exercise.config.answers[1].label )
        $('#digmit-exercise-hasAudio1').prop('checked', exercise.config.answers[0].hasAudio )
        $('#digmit-exercise-hasAudio2').prop('checked', exercise.config.answers[1].hasAudio )
      }
      else if (field.func === DigMitExercise.renderWordField) {
        // get rid of existing rows before we recreate the table from the exercise data
        let $table = $('#word-table')
        $table.children().remove()

        // define the full word/sentence vs syllables/segments part based on exercise type
        const full = exercise.type === "draggable-syllables" ? "label" : "full"
        let part
        switch ( exercise.type ) {
          case "listen-and-write":
            part = "sounds"
            break
          case "lingua-puzzle":
            part = "segments"
            break
          default:
            part = "syllables"
        }

        // now add each row in turn
        rows = exercise.config.words
        for (let i=0; i<rows.length; i++) {
          DigMitExercise.addWordRow()

          // fill in the full word/sentence
          $(`#digmit-exercise-word-${i}`).val(rows[i][full])

          // fill in the syllables/segments
          for (let s=0; s<rows[i][part].length; s++) {
            if (s > 0) DigMitExercise.addSyllableField(i)
            $(`#digmit-exercise-syllable-${i}-${s}`).val(rows[i][part][s])
          }

          // for draggable syllables we have to also fill in the chosen answer
          if (exercise.type === 'draggable-syllables') {
            if (rows[i].answer === 'answer-0') {
              $(`#digmit-exercise-answerRadio1-${i}`).prop('checked', true)
            } else if (rows[i].answer === 'answer-1') {
              $(`#digmit-exercise-answerRadio2-${i}`).prop('checked', true)
            }
          }
          // for lingua puzzles we also have to fill in the audio file
          else if (exercise.type === 'lingua-puzzle') {
            $(`#digmit-exercise-audioSource-${i}`).val(rows[i].file)
          }
        }
      }
    }
  }

  // now we have to do some specific adaptations on the "words" table, depending on exercise type
  const wordsLen = $("#word-table").find(".digmit-exercise-word-row").length

  // draggable-syllables have an extra cell on the word table, so we check here whether to add or remove those
  if (exerciseType === "draggable-syllables") {
    for (let i = 0; i <= wordsLen; i++) {
      // only add them if they are not there yet
      if ( $("#word-table").find("#digmit-exercise-answerRadio1-" + i).length === 0 ) {
        this.renderRadioAnswer(i);
      }
    }
  } else {
    $(".digmit-exercise-answerRadio1").closest('td').remove();
  }

  // same for lingua-puzzle
  if (exerciseType === "lingua-puzzle") {
    for (let i = 0; i <= wordsLen; i++) {
      // only add them if they are not there yet
      if (  $("#word-table").find("#digmit-exercise-audioSource-" + i).length === 0 ) {
        this.renderAudioFileInput(i);
      }
    }
  } else {
    $(".digmit-exercise-audioSource").closest('td').remove();
  }

  // if any words/texts already exist, we need to update the labels for words/text and syllables/segments
  if ( wordsLen > 0 ) {
    const rows = $("#word-table").find(".digmit-exercise-word-row")
    const fullLabel = exerciseType  === "lingua-puzzle" ? "Text" : "Wort"
    const partLabel = exerciseType  === "lingua-puzzle" ? "Segmente" : "Silben"
    const segmentsIndex = exerciseType === "draggable-syllables" || exerciseType === "lingua-puzzle" ? 3 : 2
    for (let i = 0; i < wordsLen; i++) {
      rows.eq(i).children().eq(0).children().eq(0).text(fullLabel)
      rows.eq(i).children().eq(segmentsIndex).children().eq(0).text(partLabel)
    }
  }

}