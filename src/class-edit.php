<?php
namespace DigmitExercises;

class Edit
{

  public static function render () {
    ?>
    <div class="wrap">
      <h1>dig_mit! Übung bearbeiten</h1>

      <div id="digmit-exercise-notifications"></div>

      Select an exercise to edit: <select id="digmit-exercises-selector"></select>
      <hr>

      <input type="hidden" id="digmit-api-url" value="<?= get_rest_url( null, DIGMIT_API_ROOT ) . '/exercise' ?>">

      <table id="form">
        <tr>
          <td colspan="2" align="center">
            <input type="hidden" id="digmit-api-nonce" value="<?= wp_create_nonce( 'wp_rest' ) ?>">
            <input type="hidden" id="digmit-link-overview" value="<?php menu_page_url("digmit_menu") ?>">
            <input type="hidden" id="digmit-link-edit" value="<?php menu_page_url("digmit_menu_edit") ?>">
          </td>
      </table>
      <div>
        <hr>
        <a href="#" id="digmit-exercise-submit-edit"><button>Speichern</button></a>
      </div>
    </div>
    <?php
  }

}
