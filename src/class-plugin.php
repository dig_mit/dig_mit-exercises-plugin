<?php
namespace DigmitExercises;

class Plugin {
  public function init () {
    add_action ( 'admin_menu', 'DigmitExercises\Plugin::add_admin_menu' );
    add_action ( 'wp_enqueue_scripts', 'DigmitExercises\Plugin::load_frontend_styles' );
    add_action ( 'wp_enqueue_scripts', 'DigmitExercises\Plugin::load_frontend_scripts' );
    add_action ( 'admin_enqueue_scripts', 'DigmitExercises\Plugin::load_backend_styles' );
    add_action ( 'admin_enqueue_scripts', 'DigmitExercises\Plugin::load_admin_scripts' );

    require_once plugin_dir_path( __FILE__ ) . 'class-exercise.php';
    add_shortcode( DIGMIT_SHORTCODE, 'DigmitExercises\Exercise::render' );

    add_action( 'rest_api_init', function () {
      require_once plugin_dir_path( __FILE__ ) . 'class-rest-controller-exercises.php';
      $controller = new REST_Controller_Exercises();
      $controller->register_routes();
    } );
  }

  public static function load_frontend_styles ()
  {
    wp_enqueue_style( 'digmit_exercises_frontend', plugin_dir_url( __FILE__ ) . '../assets/css/frontend.css' );
    wp_enqueue_style( 'font-awesome', plugin_dir_url( __FILE__ ) . '../assets/css/font-awesome-all.min.css' );
  }

  public static function load_backend_styles ()
  {
    wp_enqueue_style( 'digmit_exercises_backend', plugin_dir_url( __FILE__ ) . '../assets/css/backend.css' );
  }

  public static function load_frontend_scripts () {
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-widget' );
    wp_enqueue_script( 'jquery-ui-mouse' );
    wp_enqueue_script( 'jquery-ui-draggable' );
    wp_enqueue_script( 'jquery-ui-droppable' );
    wp_enqueue_script( 'jquery-ui-sortable' );
    wp_enqueue_script( 'jquery-ui-touch-punch', plugin_dir_url( __FILE__ ) . 'js/jquery.ui.touch-punch.js', [], false, true );
  }

  public static function load_admin_scripts ()
  {
    wp_enqueue_script( 'dimgit_exercises_admin_init', plugin_dir_url( __FILE__ ) . 'js/admin-init.js' );
    wp_enqueue_script( 'dimgit_exercises_render_form_fields', plugin_dir_url( __FILE__ ) . 'js/admin-render-form-fields.js' );
    wp_enqueue_script( 'dimgit_exercises_parse_form_fields', plugin_dir_url( __FILE__ ) . 'js/admin-parse-form-fields.js' );
    wp_enqueue_script( 'dimgit_exercises_admin_create', plugin_dir_url( __FILE__ ) . 'js/admin-create.js' );
    wp_enqueue_script( 'dimgit_exercises_admin_delete', plugin_dir_url( __FILE__ ) . 'js/admin-delete.js' );
    wp_enqueue_script( 'dimgit_exercises_admin_edit', plugin_dir_url( __FILE__ ) . 'js/admin-edit.js' );
  }

  public static function add_admin_menu ()
  {
    require_once plugin_dir_path( __FILE__ ) . 'class-overview.php';
    require_once plugin_dir_path( __FILE__ ) . 'class-create.php';
    require_once plugin_dir_path( __FILE__ ) . 'class-edit.php';
    require_once plugin_dir_path( __FILE__ ) . 'class-maintenance.php';
    require_once plugin_dir_path( __FILE__ ) . 'class-settings.php';

    add_menu_page( 'dig_mit! Übungen', 'dig_mit! Übungen',
      'manage_options', 'digmit_menu', 'DigmitExercises\Overview::render',
      'dashicons-database', 99);

    // here we use the same slug as the parent menu, to be able to have a
    // different label for the first submenu entry
    // not documented, but see https://wordpress.stackexchange.com/a/354163
    add_submenu_page( 'digmit_menu', 'Übungs-Übersicht', 'Übersicht',
      'manage_options', 'digmit_menu', 'DigmitExercises\Overview::render');

    add_submenu_page( 'digmit_menu', 'Übung Anlegen', 'Anlegen',
      'manage_options', 'digmit_menu_create', 'DigmitExercises\Create::render');

    add_submenu_page( 'digmit_menu', 'Übung Bearbeiten', 'Bearbeiten',
      'manage_options', 'digmit_menu_edit', 'DigmitExercises\Edit::render');

    add_submenu_page( 'digmit_menu', 'Übungen: Wartungsarbeiten', 'Wartungsarbeiten',
      'manage_options', 'digmit_menu_maintenance', 'DigmitExercises\Maintenance::render');

    add_submenu_page( 'digmit_menu', 'Übungen: Einstellungen', 'Einstellungen',
      'manage_options', 'digmit_menu_settings', 'DigmitExercises\Settings::render');

  }

  public static function render_error ( $msg ) {
    return '
      <div class="error">
        <span class="dashicons dashicons-warning"></span>
        <b> Error:</b><br>
        '.$msg.'
      </div>
    ';
  }

}
