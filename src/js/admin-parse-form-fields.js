
DigMitExercise.validateForm = function () {
  this.clearNotifications()
  this.validated = false

  this.exerciseType = $("#digmit-exercise-type").val()
  if (!this.exerciseType) {
    this.displayError( 'No exercise type is chosen!' )
    return false
  }

  this.exerciseConfig = {
    title: $( "#digmit-exercise-title" ).val(),
    description: $( "#digmit-exercise-description" ).val(),
    next: $( "#digmit-exercise-next" ).val(),
    nextLabel: $( "#digmit-exercise-nextLabel" ).val(),
    audioSource: $( "#digmit-exercise-audioSource" ).val(),
  }

  if (!this.exerciseConfig.title) {
    this.displayError( 'This field must not be empty: Titel' )
    return false
  }

  if (!this.exerciseConfig.description) {
    this.displayError( 'This field must not be empty: Beschreibung' )
    return false
  }

  if (!this.exerciseConfig.next) {
    this.displayError( 'This field must not be empty: Link am Ende der Übung' )
    return false
  }

  if (!this.exerciseConfig.nextLabel) {
    this.displayError( 'This field must not be empty: Weiter am Ende der Übung' )
    return false
  }

  if (!this.exerciseConfig.audioSource) {
    this.displayError( 'This field must not be empty: Audio Source' )
    return false
  }

  // several exercises have an additional property: nextWordLabel
  if ( ['syllable-puzzle', 'listen-and-write', 'lingua-puzzle'].find(item => item === this.exerciseType) ) {
    this.exerciseConfig.nextWordLabel = $( "#digmit-exercise-nextWordLabel" ).val()
    if (!this.exerciseConfig.nextWordLabel) {
      this.displayError( 'This field must not be empty: Nächstes Wort' )
      return false
    }
  }

  // the listen and write exercise has an additional property: showFullWord
  if ( this.exerciseType === 'listen-and-write' ) {
    this.exerciseConfig.showFullWord = $( "#digmit-exercise-showFullWord" ).prop('checked')
  }

  // the draggable-syllables exercise type has two additional properties: answers & titleAudio
  if ( this.exerciseType === 'draggable-syllables' ) {
    this.exerciseConfig.answers = [
      {
        'label': $( "#digmit-exercise-answer1").val(),
        'hasAudio': $( "#digmit-exercise-hasAudio1").prop('checked'),
      },
      {
        'label': $( "#digmit-exercise-answer2").val(),
        'hasAudio': $( "#digmit-exercise-hasAudio2").prop('checked'),
      }
    ]
    if (!this.exerciseConfig.answers[0].label) {
      this.displayError( 'This field must not be empty: Antwort 1' )
      return false
    }
    if (!this.exerciseConfig.answers[1].label) {
      this.displayError( 'This field must not be empty: Antwort 2' )
      return false
    }
    this.exerciseConfig.titleAudio = {
      'enabled': $( "#digmit-exercise-titleAudioEnabled").prop('checked'),
      'label': $( "#digmit-exercise-titleAudio").val()
    }
    if (this.exerciseConfig.titleAudio.enabled && !this.exerciseConfig.titleAudio.label) {
      this.displayError( 'This field must not be empty: Label für Audio im Titel' )
      return false
    }
  }

  // check the word table for some mandatory fields
  rows = $("#word-table").find(".digmit-exercise-word")
  for (let wid = 0; wid < rows.length; wid++) {
    if (!rows[wid].value) {
      this.displayError( `There is an empty word at position ${wid+1}` )
      return false
    }

    if (this.exerciseType === 'draggable-syllables') {
      if (
        !$(`#digmit-exercise-answerRadio1-${wid}`).prop('checked') &&
        !$(`#digmit-exercise-answerRadio2-${wid}`).prop('checked')
      ) {
        this.displayError( `The correct answer has to be chosen for: Wort ${rows[wid].value}` )
        return false
      }
    }

    if (this.exerciseType === 'lingua-puzzle' ) {
      if (!$(`#digmit-exercise-audioSource-${wid}`).val()) {
        this.displayError( `The audio file is missing for: Wort ${rows[wid].value}` )
        return false
      }
    }
  }

  this.exerciseData = {
    type: this.exerciseType,
    note: $( "#digmit-exercise-note" ).val(),
    config: this.exerciseConfig
  }

  // a somewhat hacky way to enforce form validation before this.parseForm() is called
  this.validated = true
  setTimeout(() => {this.validated = false}, 500)
}


DigMitExercise.parseForm = function () {
  if (!this.validated) {
    this.displayError( 'Cannot parse form, because it is not validated!' )
    return
  }

  // build the words-array
  let words = []
  let rows = $("#word-table").find(".digmit-exercise-word")
  for (let wid = 0; wid < rows.length; wid++) {
    syllables = [];
    let silbenliste = $(`#digmit-exercise-word-row-${wid}`).find('.digmit-exercise-syllable')
    for (let sid = 0; sid < silbenliste.length; sid++) {
      syllables.push(silbenliste[sid].value)
    }

    const fullword = this.exerciseType === "draggable-syllables" ? "label" : "full"

    let wordpart
    switch ( this.exerciseType ) {
      case "listen-and-write":
        wordpart = "sounds"
        break

      case "lingua-puzzle":
        wordpart = "segments"
        break

      default:
        wordpart = "syllables"
    }

    let word = {
      [fullword]: rows[wid].value,
      [wordpart]: syllables
    }

    // some exercise types have additional properties in the word objects
    if ( this.exerciseType === "draggable-syllables" ) {
      word.answer = $(`#digmit-exercise-answerRadio1-` + wid).prop('checked') ? "answer-0" : "answer-1"
    }
    else if ( this.exerciseType === "lingua-puzzle" ) {
      word.file = $(`#digmit-exercise-audioSource-${wid}`).val()
    }

    words.push(word)
  }

  // the words are added as the last parameter for editing convenience in the raw json string
  this.exerciseData.config.words = words

  // when gathering the config object, only allow valid json that can be parsed
  try {
    this.exerciseData = JSON.parse(JSON.stringify(this.exerciseData));
  } catch ( err ) {
    this.displayError( 'Your exercise data could not be parsed as valid JSON' )
    return
  }
}