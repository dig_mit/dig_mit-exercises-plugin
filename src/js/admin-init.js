let $ = jQuery

const DigMitExercise = {

  displayError ( msg ) {
    let html = '<div class="error">'
    html += '<span class="dashicons dashicons-warning"></span>'
    html += '<b> Error:</b><br>'
    html += msg
    html += '<div>'
    $( html ).appendTo( $( '#digmit-exercise-notifications' ) )
  },

  displayInfo ( msg ) {
    let html = '<div class="success">'
    html += '<span class="dashicons dashicons-yes"></span>'
    html += msg
    html += '<div>'
    $( html ).appendTo( $( '#digmit-exercise-notifications' ) )
  },

  clearNotifications () {
    $( '#digmit-exercise-notifications' ).children().remove()
  },

  initOverview () {
    $( ".digmit-exercise-shortcode-copy" ).on( "click", event => {
      navigator.clipboard.writeText( $( event.target ).data('code') )
    })
  }

}


$( function () {
  let getTemp = window.location.search.substring(1).split('&')
  DigMitExercise.getParams = {}
  for ( let param of getTemp ) {
    param = param.split( '=', 2 )
    DigMitExercise.getParams[ param[0] ] = param[1]
  }

  switch ( DigMitExercise.getParams.page ) {
    case 'digmit_menu':
      DigMitExercise.initOverview()
      DigMitExercise.initDelete()
      break;

    case 'digmit_menu_create':
      DigMitExercise.initCreate()
      break;

    case 'digmit_menu_edit':
      DigMitExercise.initEdit()
      break;
  }

})
