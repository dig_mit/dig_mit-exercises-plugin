<?php
namespace DigmitExercises;
use WP_REST_Controller;
use WP_REST_Server;
use WP_REST_Response;
use WP_Error;

class REST_Controller_Exercises extends WP_REST_Controller {

  public function __construct() {
    $this->namespace = DIGMIT_API_ROOT;
    $this->resource  = 'exercise';
  }

  public function register_routes() {
    register_rest_route( $this->namespace, '/' . $this->resource, array(
      array(
        'methods'             => WP_REST_Server::READABLE,
        'callback'            => array( $this, 'get_items' ),
        'permission_callback' => array( $this, 'get_items_permissions_check' ),
      ),
      array(
        'methods'             => WP_REST_Server::CREATABLE,
        'callback'            => array( $this, 'create_item' ),
        'permission_callback' => array( $this, 'create_item_permissions_check' ),
      ),
    ) );

    register_rest_route( $this->namespace, '/' . $this->resource . '/(?P<id>[\d]+)', array(
      array(
        'methods'             => WP_REST_Server::READABLE,
        'callback'            => array( $this, 'get_item' ),
        'permission_callback' => array( $this, 'get_items_permissions_check' ),
      ),
      array(
        'methods'             => WP_REST_Server::EDITABLE,
        'callback'            => array( $this, 'update_item' ),
        'permission_callback' => array( $this, 'update_item_permissions_check' ),
      ),
      array(
        'methods'             => WP_REST_Server::DELETABLE,
        'callback'            => array( $this, 'delete_item' ),
        'permission_callback' => array( $this, 'delete_item_permissions_check' ),
      ),
    ) );

    /*
    TODO: reactivate this when we have a schema defintion
    register_rest_route( $this->namespace, '/' . $this->resource . '/schema', array(
      'methods'  => WP_REST_Server::READABLE,
      'callback' => array( $this, 'get_public_item_schema' ),
    ) );
    */
  }

  public function get_items_permissions_check( $request ) {
    return true;
  }
  public function get_item_permissions_check( $request ) {
    return true;
  }
  public function create_item_permissions_check( $request ) {
    return current_user_can( 'manage_options' );
  }
  public function update_item_permissions_check( $request ) {
    return current_user_can( 'manage_options' );
  }
  public function delete_item_permissions_check( $request ) {
    return current_user_can( 'manage_options' );
  }

  /**
   * Retrieve a list of all exercises
   *
   * @param WP_REST_Request $request Full request data
   * @return WP_Error|WP_REST_Response
   */
  public function get_items( $request ) {
    global $wpdb;

    // load all exercises from database
    $data = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . DIGMIT_TABLE_EXERCISES );

    foreach ( $data as $item ) {
      $item->config = json_decode( $item->config );
    }

    return new WP_REST_Response( $data, 200 );
  }

  /**
   * Retrieve an exercise
   *
   * @param WP_REST_Request $request Full request data
   * @return WP_Error|WP_REST_Response
   */
  public function get_item ( $request ) {
    global $wpdb;
    $params = $request->get_params();

    $item = $this->get_exercise( $params["id"] );

    if ( empty( $item ) ) {
      return new WP_Error( 'not_found', 'No exercise with this ID could be found', [ "status" => 404 ] );
    }

    $item->config = json_decode( $item->config );

    return new WP_REST_Response( $item, 200 );
  }

  /**
   * Create an exercise
   *
   * @param WP_REST_Request $request Full request data
   * @return WP_Error|WP_REST_Response
   */
  public function create_item ( $request ) {
    $params = $request->get_json_params();

    $v = $this->validate_exercise( $params );
    if ( $v !== true ) {
      // in this case $v will be a WP_Error which we can return directly
      return $v;
    }

    global $wpdb;
    $table = $wpdb->prefix . DIGMIT_TABLE_EXERCISES;
    $insert_data = [
      "type" => $params["type"],
      "config" => json_encode( $params["config"] ),
      "note" => sanitize_text_field( $params["note"] ),
    ];
    $inserted = $wpdb->insert( $table, $insert_data );

    if ( ! $inserted ) {
      return new WP_Error( 'insert_error', 'Could not insert new exercise into database table', [ "status" => 500 ] );
    }

    $params["id"] = $wpdb->insert_id;
    return new WP_REST_Response( $params, 200 );
  }

  /**
   * Update an exercise
   *
   * @param WP_REST_Request $request Full request data
   * @return WP_Error|WP_REST_Response
   */
  public function update_item ( $request ) {
    $id = $request->get_url_params()["id"];

    if ( ! $this->exercise_exists( $id ) ) {
      return new WP_Error( 'not_found', 'No exercise with this ID could be found', [ "status" => 404 ] );
    }

    $params = $request->get_json_params();
    if ( isset( $params["id"] ) && $params["id"] !== $id ) {
      return new WP_Error( 'id_error', 'The ID in your JSON body does not match the requested ID', [ "status" => 400 ] );
    }

    $v = $this->validate_exercise( $params );
    if ( $v !== true ) {
      // in this case $v will be a WP_Error which we can return directly
      return $v;
    }

    global $wpdb;
    $table = $wpdb->prefix . DIGMIT_TABLE_EXERCISES;
    $update_data = [
      "type" => $params["type"],
      "config" => json_encode( $params["config"] ),
      "note" => sanitize_text_field( $params["note"] ),
    ];
    $where = [
      "id" => $id,
    ];
    $updated = $wpdb->update( $table, $update_data, $where );

    // here we only throw an error if the updated resulted in false
    // if 0 lines have been updated, this only means the data did not change
    if ( $updated === false ) {
      return new WP_Error( 'update_error', 'Could not update exercise in database table', [ "status" => 500 ] );
    }

    $params["id"] = $id;  // in case it was not already set in the request body
    return new WP_REST_Response( $params, 200 );
  }

  /**
   * Delete an exercise
   *
   * @param WP_REST_Request $request Full request data
   * @return WP_Error|WP_REST_Response
   */
  public function delete_item ( $request ) {
    $params = $request->get_params();

    if ( ! $this->exercise_exists( $params["id"] ) ) {
      return new WP_Error( 'not_found', 'No exercise with this ID could be found', [ "status" => 404 ] );
    }

    global $wpdb;
    $table = $wpdb->prefix . DIGMIT_TABLE_EXERCISES;
    $where = [
      "id" => $params["id"],
    ];
    $deleted = $wpdb->delete( $table, $where );

    if ( ! $deleted ) {
      return new WP_Error( 'delete_error', 'Could not delete exercise from database table', [ "status" => 500 ] );
    }

    return new WP_REST_Response( null, 204 );
  }

  /**
   * Checks whether an exercise exists in the database.
   *
   * @param int $id The ID of the exercise to be checked.
   * @return bool true if the exercise exists
   */
  private function exercise_exists ( $id ) {
    global $wpdb;
    $table = $wpdb->prefix . DIGMIT_TABLE_EXERCISES;
    $query = $wpdb->prepare( "SELECT id FROM $table WHERE id = %d", $id );
    $item = $wpdb->get_row( $query );
    if ( empty( $item ) ) {
      return false;
    }
    return true;
  }

  /**
   * Retrieves an exercise from the database.
   *
   * @param int $id The ID of the exercise to be retrieved.
   * @return object|null An object containing the results or null if not found.
   */
  private function get_exercise ( $id ) {
    global $wpdb;
    $table = $wpdb->prefix . DIGMIT_TABLE_EXERCISES;
    $query = $wpdb->prepare( "SELECT * FROM $table WHERE id = %d", $id );
    $item = $wpdb->get_row( $query );
    return $item;
  }

  /**
   * Validates an exercise that was passed in a POST or PUT request.
   *
   * Returns either true on successfull validation, or a WP_Error instance.
   * While validating, the $params array will be modified, where necessary.
   * Default values are added, if not submitted (e.g. an empty string for note).
   *
   * @param array &$request Reference to the parameter array, which will be validated
   * @return WP_Error|true
   */
  private function validate_exercise ( &$params ) {
    // We don't have to validate the JSON itself, this was already handled
    // by get_json_params (an error will be sent back if invalid JSON was sent).
    // But we want to send an error on empty POSTs.
    if ( empty( $params ) ) {
      return new WP_Error( 'empty_post', 'Your POST body is empty', [ "status" => 400 ] );
    }

    if ( empty( $params["type"] ) ) {
      return new WP_Error( 'type_missing', 'You have to specify an exercise type', [ "status" => 400 ] );
    }

    if ( empty( $params["config"] ) ) {
      return new WP_Error( 'config_missing', 'You have to specify an exercise config', [ "status" => 400 ] );
    }

    if ( ! in_array( $params["type"], DIGMIT_IMPLEMENTED_TYPES ) ) {
      return new WP_Error( 'wrong_type', 'This type of exercise is not supported', [ "status" => 400 ] );
    }

    // TODO: implement proper exercise config validation
    // for now we just check if it is an array and at least has some of the fields
    // required for every exercise
    if ( ! is_array( $params["config"] ) ) {
      return new WP_Error( 'config_error', 'Your exercise config is not an array', [ "status" => 400 ] );
    }
    $required = [ "title", "description", "next", "nextLabel", "audioSource" ];
    foreach ( $required as $field ) {
      if ( empty ( $params["config"][$field] ) ) {
        return new WP_Error( 'config_field_error', 'Config parameter '.$field.' is missing or empty', [ "status" => 400 ] );
      }
    }

    // an unset note is treated as an empty string
    if ( ! isset( $params["note"] ) ) {
      $params["note"] = "";
    }

    return true;
  }

}
