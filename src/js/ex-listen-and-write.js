let $ = jQuery
let $container = $( '#digmit-exercise-container-'+digMitExerciseID  )

class ListenAndWrite {
  constructor( config ) {
    this.config = config
  }

  init () {
    this.initialiseContainer()
    this.initialiseHeader()
    this.initialiseWord( this )
    $( "#next-word-button" ).on( "click", { self: this }, this.prepareNextWord )
    $( "#next-word-button span" ).text( this.config.nextWordLabel )
    $( "#next-exercise-button span" ).text( this.config.nextLabel )
    $( "#next-exercise-button" ).on( "click", () => {
      window.location = this.config.next
    })
  }

  initialiseContainer () {
    $( '<div class="header"><h2></h2></div>' ).appendTo( $container )
    $( '<div class="description"></div>' ).appendTo( $container )
    $( '<div class="target-field single-target-area height6"></div>' ).appendTo( $container )

    let successMessage = `
      <div class="success-message" style="display: none">
        <p>
          Super!
        </p>
        <div id="next-exercise-button">
          <span></span>
          <i class="fa fa-arrow-circle-right"></i>
        </div>
      </div>
    `
    $( successMessage ).appendTo( $container )

    let nextWordMessage = `
      <div class="next-word-message" style="display: none">
        <p>
          Super!
        </p>
        <div id="next-word-button">
          <span></span>
          <i class="fa fa-arrow-circle-right"></i>
        </div>
      </div>
    `
    $( nextWordMessage ).appendTo( $container )

  }

  initialiseHeader () {
    let config = this.config
    $( "title" ).text( config.title )
    $( ".header h2" ).text( config.title )
    $( ".description" ).text( config.description )
  }

  // take the next word from config.words array, create elements for the
  // audio buttons and the inputs and place them in the target field
  initialiseWord ( self ) {
    let config = self.config
    // check if this is the frist word in the exercise and create counter
    // otherwise increase counter for the next word
    if ( config.counter === undefined ) {
      config.counter = 0;
    } else {
      config.counter++
    }

    let word = config.words[ config.counter ]
    // if it is configured, we add a button to listen to the full word
    if ( config.showFullWord ) {
      let $wordButton = self.createFullWord( self, word.full )
      $wordButton.insertBefore( $( ".target-field") )
    }
    // now add the input boxes with the input elements
    let char = 0
    for ( let i in word.sounds ) {
      let $div = $( '<div class="input-box"></div>' )
      // we only need one input field for sounds consisting of one character
      if ( word.sounds[i].length === 1 ) {
        $( '<input type="text" maxlength="1" id="exercise-input-'+char+'">' ).
          appendTo( $div ).
          on( "input", { position: char, self: self }, self.processInput ).
          on( 'focus', { label: word.sounds[i].toLowerCase(), source: config.audioSource } , self.playAudio ).
          on( 'focus click', event => $(this).select() ).
          on( 'mouseup', event => event.preventDefault() )
        char++
      } else {
      // for sounds with more characters we add more input elements
        for ( let j in word.sounds[i] ) {
          $( '<input type="text" maxlength="1" id="exercise-input-'+char+'">' ).
            appendTo( $div ).
            on( "input", { position: char, self: self }, self.processInput ).
            on( 'focus', { label: word.sounds[i][j].toLowerCase(), source: config.audioSource } , self.playAudio ).
            on( 'focus click', event => $(this).select() ).
            on( 'mouseup', event => event.preventDefault() )
          char++
        }
      }
      // add an audio button to listen to the sound
      $( '<br>' ).appendTo( $div )
      $( '<i class="fa fa-volume-up"></i>' ).appendTo( $div ).
        on( "click", { label: word.sounds[i].toLowerCase(), source: config.audioSource } , self.playAudio )
      // add an info symbol with a solution
      $( '<br>' ).appendTo( $div )
      $( '<i class="help fa fa-question-circle"></i>' ).appendTo( $div ).
        on( 'click', event => {
          $( '#solution-'+i ).toggle( 'fade' )
        })
      $( '<div class="solution"><span id="solution-'+i+'">'+word.sounds[i]+'</span></div>' ).appendTo( $div )
      // now append the whole div to the exercise target area
      $div.appendTo( $( ".target-field") )
    }
  }

  // play an audio file associated with a word
  playAudio ( event ) {
    let source = event.data.source
    let label = event.data.label
    let audio = new Audio( source + label + '.mp3' )
    audio.play()
  }

  // creates and returns a clickable and non-draggable button with a text
  // that is played back when the user clicks on it
  createFullWord ( self, text ) {
    let el = '<div class="full-word"><i class="fa fa-volume-up"></i></div>'
    let $el = $( el )
    $el.on( "click", { label: text, source: self.config.audioSource } , self.playAudio )
    return $el
  }

  processInput ( event ) {
    let self = event.data.self
    let config = self.config
    let position = event.data.position
    let value = event.originalEvent.data
    let word = config.words[ config.counter ]
    let $input = $( event.target )

    // check if the new character fits the position and apply CSS classes
    if ( value === null ) {
      $input.removeClass( "correct incorrect" )
    } else {
      // the answer should be correct even if the case does not match
      if ( value.toLowerCase() === word.full[ position ].toLowerCase() ) {
        // but we want to fill in the correct case anyway
        $input.val( word.full[ position ] )
        $input.addClass( "correct" ).removeClass( "incorrect" )
        // if the input character was correct and we are not in the last field
        // then switch the focus to the next character
        if ( position < word.full.length - 1 ) {
          $( "#exercise-input-"+(position+1) ).focus()
        }
      } else {
        $input.addClass( "incorrect" ).removeClass( "correct" )
      }
    }

    // check if all items are already placed correctly and set visibility for
    // the next word or success message accordingly
    if ( self.allCorrect( word ) ) {
      // deactivate all input elements
      $( ".input-box input" ).attr( "disabled", "true" )
      $( '<span id="current-word"></span>' ).prependTo( $( ".full-word") )
      $( "#current-word" ).text( word.full )
      // if the audio button to listen to the full word was not shown so far, add it now
      if ( ! config.showFullWord ) {
        let $wordButton = self.createFullWord( self, word.full )
        $wordButton.insertBefore( $( ".target-field") )
        $( '<span id="current-word"></span>' ).prependTo( $( ".full-word") )
        $( "#current-word" ).text( word.full )

      }
      // move the whole word and it's audio button after the single letters
      $( ".full-word" ).insertAfter( $( ".target-field") )
      // now check if this was already the last word in the exercise
      if ( config.counter + 1 >= config.words.length ) {
        $( ".success-message" ).css( "display", "block" )
      } else {
        $( ".next-word-message" ).css( "display", "block" )
      }
    } else {
      $( ".success-message" ).css( "display", "none" )
    }
  }

  // check if all fields have been filled correctly and return true or false
  allCorrect ( word ) {
    for ( let i = 0; i < word.full.length; i++ ) {
      let value = $( "#exercise-input-"+i ).val()
      if ( value === "" || value !== word.full[ i ] ) {
        return false
      }
    }
    return true
  }

  // this is called after a word was solved to prepare for the next word in the
  // exercise. we have to restore the target field accordingly
  prepareNextWord ( event ) {
    let self = event.data.self
    $( ".target-field" ).children().remove()
    $( ".full-word" ).remove()
    $( ".next-word-message" ).css( "display", "none" )
    self.initialiseWord( self )
  }

}

// start initialisation as soon as the document is ready
// `$( function() )` is an alias of `$( document ).ready( function() )`
$( function() {
  let exercise = new ListenAndWrite( DigMitConfig )
  exercise.init()
});
