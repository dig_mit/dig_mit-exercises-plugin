let $ = jQuery
let $container = $( '#digmit-exercise-container-'+digMitExerciseID  )

class SyllablePuzzle {
  constructor( config ) {
    this.config = config
  }

  init () {
    this.initialiseContainer()
    this.initialiseHeader()
    this.initialiseSyllables( this )
    this.initialiseTargets( this )
    $( "#next-word-button" ).on( "click", { self: this }, this.prepareNextWord )
    $( "#next-word-button span" ).text( this.config.nextWordLabel )
    $( "#next-exercise-button span" ).text( this.config.nextLabel )
    $( "#next-exercise-button" ).on( "click", () => {
      window.location = this.config.next
    })
  }

  initialiseContainer () {
    $( '<div class="header"><h1></h1></div>' ).appendTo( $container )
    $( '<div class="description"></div>' ).appendTo( $container )
    $( '<div class="source-field"></div>' ).appendTo( $container )

    $( '<div class="target-field single-target-area height6"></div>' ).appendTo( $container )

    let nextWordMessage = `
    <div class="next-word-message" style="display: none">
        <p>Super!</p>
      <div id="next-word-button">
        <span></span>
        <i class="fa fa-solid fa-play"></i>
      </div>
    </div>
    `

    $( nextWordMessage ).appendTo( $container )

    let successMessage = `
      <div class="success-message" style="display: none">
        <p>
          Super!
        </p>
        <div id="next-exercise-button">
          <span></span>
          <i class="fa fa-solid fa-play"></i>
        </div>
      </div>

    `
    $( successMessage ).appendTo( $container )
  }

  initialiseHeader () {
    let config = this.config
    $( "title" ).text( config.title )
    $( ".header h1" ).text( config.title )
    $( ".description" ).text( config.description )
  }

  // take the next word from config.words array, create elements for the syllables
  // and place them in the source area
  initialiseSyllables ( self ) {
    let config = self.config
    // check if this is the frist word in the exercise and create counter
    // otherwise increase counter for the next word
    if ( config.counter === undefined ) {
      config.counter = 0;
    } else {
      config.counter++
    }

    // shuffle the syllables and make sure the first one is not the first of the word
    let unshuffled = config.words[ config.counter ].syllables
    // make a copy to not manipulate the original array
    unshuffled = unshuffled.slice( 0, unshuffled.length )
    let shuffled = []
    while ( unshuffled.length > 0 ) {
      let i = Math.floor( Math.random() * unshuffled.length );
      // an index of 0 is only ok, if we're not filling the first syllable
      if ( shuffled.length === 0 && i === 0 ) {
        continue;
      }
      shuffled.push( unshuffled.splice( i, 1 )[0] )
    }

    // first add a non-draggable audio button for the whole word
    let $el = self.createFullWord( self, config.words[ config.counter ].full )
    $el.insertAfter( $( ".description" ) )

    // now we can add all words as draggables to the source field
    for ( let i in shuffled ) {
      let el = '<div class="audio-button" id="syllable-'+i+'">' + shuffled[i] + '<br><i class="fa fa-volume-up"></i></div>'
      $el = $( el )
      $el.appendTo( $( ".source-field" ) )
      $el.draggable({ revert: true })
      $el.on( "click", { label: shuffled[i], source: config.audioSource } , self.playAudio )
      i++
    }

    // allow audio buttons to be moved back to the source field
    $( ".source-field" ).droppable({
      accept: ".audio-button",
      hoverClass: "drop-hover",
    }).on( 'drop', { self: self }, self.processDrop )
  }

  // initialise all target fields as droppables
  initialiseTargets ( self ) {
    let config = self.config
    let syllables = config.words[ config.counter ].syllables
    for ( let i = 0; i < syllables.length; i++ ) {
      let el = '<div id="target-'+i+'"></div>'
      let $el = $( el )
      $el.appendTo( $( ".target-field" ) )
      $el.droppable({
        accept: ".audio-button",
        hoverClass: "drop-hover",
      }).on( 'drop', { self: self }, self.processDrop )
    }
  }

  // play an audio file associated with a word
  playAudio ( event ) {
    let source = event.data.source
    let label = event.data.label
    let audio = new Audio( source + label + '.mp3' )
    audio.play()
  }

  // creates and returns a clickable and non-draggable button with a text
  // that is played back when the user clicks on it
  createFullWord ( self, text ) {
    let el = '<div class="full-word"><i class="fa fa-volume-up"></i></div>'
    let $el = $( el )
    $el.on( "click", { label: text, source: self.config.audioSource } , self.playAudio )
    return $el
  }

  // when a syllable was dropped into one of the targets, we'll process it here:
  // 1. move the element to the target area
  // 2. check if the answer is correct and add corresponding class
  processDrop ( event, ui ) {
    let self = event.data.self
    let config = self.config
    let word = config.words[ config.counter ]
    let $target = $( event.target )
    let $syllable = ui.draggable

    // move the draggable to the target area
    $syllable.appendTo( $target )

    // if the draggable was moved back to the source field we just have
    // to remove the checkmark and are done here
    if ( $target.hasClass( "source-field" ) ) {
      // first we remove the <i> with the checkmark
      $syllable.children().last().remove()
      // then the <br> that is still there
      $syllable.children().last().remove()
      return
    }

    // get the numeric place of the target syllable (the id part after "target-")
    let i = parseInt( $target.attr( "id" ).substr( 7 ) )
    // if the string already as a checkmark or cross, remove it first
    let html = $syllable.html()
    if ( html.endsWith( '<br><i class="fa fa-check correct"></i>' ) ) {
      html = html.substr(0, html.length - '<br><i class="fa fa-check correct"></i>'.length )
    } else if ( html.endsWith( '<br><i class="fa fa-times incorrect"></i>' ) ) {
      html = html.substr(0, html.length - '<br><i class="fa fa-times incorrect"></i>'.length )
    }
    // now add a checkmark of cross depending on whether it is placed correctly
    if ( $syllable.text() === word.syllables[i] ) {
      html += '<br><i class="fa fa-check correct"></i>'
    } else {
      html += '<br><i class="fa fa-times incorrect"></i>'
    }
    // set the new html content
    $syllable.html( html )

    // check if all items are already placed correctly and set visibility for
    // the next word or success message accordingly
    if ( self.allCorrect( word ) ) {
      // remove the draggable from the audio buttons and add exercise-solved class
      $( ".audio-button" ).draggable( "destroy" )
      $( ".audio-button" ).addClass( "exercise-solved" )
      $( ".target-field" ).addClass( "exercise-solved" )
      // remove the source field and move the audio button for the whole word into the target area
      $(".source-field").remove()
      $( ".full-word" ).insertAfter( $( ".target-field" ) )
      // now check if this was already the last word in the exercise
      if ( config.counter + 1 >= config.words.length ) {
        $( ".success-message" ).css( "display", "block" )
        $( ".success-message a" ).attr( "href", config.next )
      } else {
        $( ".next-word-message" ).css( "display", "block" )
      }
    } else {
      $( ".success-message" ).css( "display", "none" )
    }
  }

  // check if all fields have been placed in their final position
  // and return true or false accordingly
  allCorrect ( word ) {
    // if there are still items in the source field, we are not done
    if ( $(".source-field").first().children().length !== 0 ) {
      return false
    }
    // now check for each syllable, if the target is placed correctly
    let correct = true
    for ( let i in word.syllables ) {
      if ( $( "#target-"+i ).children().first().text() !== word.syllables[i] ) {
        correct = false
        break
      }
    }
    if ( correct ) return true
    return false
  }

  // this is called after a word was solved to prepare for the next word in the
  // exercise. we have to restore source and target fields accordingly
  prepareNextWord ( event ) {
    let self = event.data.self
    // make the next word messsage invisible again
    $( ".next-word-message" ).css( "display", "none" )
    // remove the audio button
    $( ".full-word" ).remove()
    // remove the solved class from target field
    $( ".target-field" ).removeClass( "exercise-solved" )
    // let's also remove all elements from the target field
    $( ".target-field" ).children().remove()
    // and insert a new source field
    let el = '<div class="source-field"></div>'
    $( el ).insertAfter( $( ".description" ) )
    // now lets re-initialise
    self.initialiseSyllables( self )
    self.initialiseTargets( self )
  }

}

// start initialisation as soon as the document is ready
// `$( function() )` is an alias of `$( document ).ready( function() )`
$( function() {
  let exercise = new SyllablePuzzle( DigMitConfig )
  exercise.init()
});
